<%@page import="pl.javastart.model.Book" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Wynik operacji</title>
</head>
<body>
<% Book book = (Book) request.getAttribute("book"); %>
<h1>Wynik zapytania <%= request.getAttribute("option")%>
</h1>
<p>W wyniku Twojego zapytania otrzymano następujący wynik:</p>
<p>Tytuł: <%=book.getTitle()%><br>
<p>ISBN: <%=book.getIsbn()%><br>
<p>Opis: <%=book.getDescription()%></p>
</body>
</html>
