package pl.javastart.dao;

import pl.javastart.model.User;
import pl.javastart.util.ConnectionProvider;
import pl.javastart.util.DbOperationException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MysqlUserDao implements UserDao {

    private final static String CREATE = "INSERT INTO user (pesel, firstname, lastname) VALUES (?,?,?);";
    private final static String READ = "SELECT * FROM user WHERE pesel = ?;";
    private final static String UPDATE = "UPDATE user SET pesel = ?, firstname = ?, lastname = ? WHERE pesel = ?;";
    private final static String DELETE = "DELETE FROM user WHERE pesel = ?;";

    @Override
    public void create(User user) {
        try (Connection connection = ConnectionProvider.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(CREATE);) {
            preparedStatement.setString(1, user.getPesel());
            preparedStatement.setString(2, user.getFirstName());
            preparedStatement.setString(3, user.getLastName());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DbOperationException(e);
        }
    }

    @Override
    public User read(String pesel) {
        User resultUser = null;
        try (Connection connection = ConnectionProvider.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(READ);) {
            preparedStatement.setString(1, pesel);
            ResultSet resultSet = preparedStatement.executeQuery();

//            tu nadaje nazyw kolumn z db
            if (resultSet.next()) {
                resultUser = new User();
                resultUser.setPesel(resultSet.getString("pesel"));
                resultUser.setFirstName(resultSet.getString("firstname"));
                resultUser.setLastName(resultSet.getString("lastname"));
            }
        } catch (SQLException e) {
            throw new DbOperationException(e);
        }
        return resultUser;
    }

    @Override
    public void update(User user) {
        try (Connection connection = ConnectionProvider.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);) {
            preparedStatement.setString(1, user.getPesel());
            preparedStatement.setString(2, user.getFirstName());
            preparedStatement.setString(3, user.getLastName());
            preparedStatement.setString(4, user.getPesel());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DbOperationException(e);
        }
    }

    @Override
    public void delete(User user) {
        try (Connection connection = ConnectionProvider.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE);) {
            preparedStatement.setString(1, user.getPesel());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DbOperationException(e);
        }
    }
}
