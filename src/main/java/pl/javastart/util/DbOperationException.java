package pl.javastart.util;

public class DbOperationException extends RuntimeException{
    public DbOperationException(Throwable cause){
        super(cause);
    }
}
